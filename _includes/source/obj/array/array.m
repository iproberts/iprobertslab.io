classdef array < handle
    properties
        Na = 0; % number of elements in array
        x = []; % relative x coordinates of array elements (in wavelengths)
        y = []; % relative y coordinates of array elements (in wavelengths)
        z = []; % relative z coordinates of array elements (in wavelengths)
        ep = 1; % antenna pattern of each array element
        weights = []; % analog beamforming weights (complex)
        marker = 'kx'; % default element marker when plotting
        element_gain_dB;
        element_gain_linear;
    end
    methods
        function obj = array(Na,M)
            % ARRAY Creates an instance of an array object.
            % 
            % Usage:
            %  obj = ARRAY() Creates an empty array object.
            %
            %  obj = ARRAY(Na) Creates an array object where Na array
            %  elements are uniformly spaced one-half wavelength apart 
            %  along the x-axis.
            %
            % Args:
            %  Na: number of elements in the array (optional)
            % 
            % Returns:
            %  obj: an array object
            if nargin > 1
                N = Na;
                obj.Na = N*M;
                obj.x = [];
                obj.z = [];
                xx = (0:N-1)/2;
                for i = 1:M
                    obj.x = [obj.x xx]; % create a UPA
                    obj.z = [obj.z ones(1,N).*(i-1)*1/2];
                end
                obj.y = zeros(1,obj.Na);
                obj.weights = ones(obj.Na,1);
            elseif nargin > 0
                obj.Na = Na;
                obj.x = (0:Na-1)/2; % create a ULA along x
                obj.y = zeros(1,Na);
                obj.z = zeros(1,Na);
                obj.weights = ones(Na,1);
            end
        end
        
        function add_element(obj,x,y,z)
            % ADD_ELEMENT Add an element to the array.
            % 
            % Usage:
            %  ADD_ELEMENT(x,y,z)
            % 
            % Args:
            %  x: a scalar or vector of scalars of element x-coordinate(s)
            %  y: a scalar or vector of scalars of element y-coordinate(s)
            %  z: a scalar or vector of scalars of element z-coordinate(s)
            %
            % If x, y, and z are not of equal length, an error is thrown.
            if length(x) == length(y) && length(x) == length(z)
                for i = 1:length(x)
                    obj.x = [obj.x, x(i)];
                    obj.y = [obj.y, y(i)];
                    obj.z = [obj.z, z(i)];
                    obj.Na = obj.Na + 1;
                    obj.weights = [obj.weights; 1];
                end
            else
                error('x, y, and z must be the same length.');
            end
        end
        
        function remove_element(obj,idx)
            % REMOVE_ELEMENT Removes an element from the array.
            %
            % Usage:
            %  REMOVE_ELEMENT() Removes the last element from the array.
            %  REMOVE_ELEMENT(idx) Removes element at index idx from the
            %  array.
            % 
            % Args:
            %  idx: an element index to remove from the array (optional)
            if nargin < 2
                idx = obj.Na;
            else
                if idx > obj.Na
                    error('Element index must be less than or equal to the number of elements in the array.');
                elseif idx < 1
                    error('Element index must be at least 1.');
                end
            end
            obj.x = [obj.x(1:idx-1) obj.x(idx+1:end)];
            obj.y = [obj.y(1:idx-1) obj.y(idx+1:end)];
            obj.z = [obj.z(1:idx-1) obj.z(idx+1:end)];
            obj.Na = obj.Na - 1;
        end
        
        function set_marker(obj,marker)
            % SET_MARKER Sets the marker of the array elements used when
            % plotting them.
            %
            % Usage:
            %  SET_MARKER(marker)
            %
            % Args:
            %  marker: either 'transmit' or 'receive' or a custom plot
            %          marker like 'kx' or 'ko'
            if strcmp(marker,'transmit')
                marker = 'bx'; % blue x for transmit arrays
            elseif strcmp(marker,'receive')
                marker = 'rx'; % red x for receive arrays
            end
            obj.marker = marker;
        end
        
        function translate_array(obj,x,y,z)
            % TRANSLATE_ARRAY Moves an array's location by some change in
            % the x, y, and z directions.
            %
            % Usage:
            %  TRANSLATE_ARRAY(x,y,z)
            %
            % Args:
            %  x: meters to move in the x direction
            %  y: meters to move in the y direction
            %  z: meters to move in the z direction
            obj.x = obj.x + x;
            obj.y = obj.y + y;
            obj.z = obj.z + z;
        end
        
        function rotate_array(obj,tilt_x,tilt_y,tilt_z)
            % ROTATE_ARRAY Rotates the array in 3-D space. 
            %
            % Usage:
            %  ROTATE_ARRAY(tilt_x,tilt_y,tilt_z)
            %
            % Args:
            %  tilt_x: tilt around the x-axis (radians)
            %  tilt_y: tilt around the y-axis (radians)
            %  tilt_z: tilt around the z-axis (radians)
            for i = 1:obj.Na
                x = obj.x(i);
                y = obj.y(i);
                z = obj.z(i);
                [u,v,w] = rotate_cartesian_point(x,y,z,tilt_x,tilt_y,tilt_z);
                obj.x(i) = u;
                obj.y(i) = v;
                obj.z(i) = w;
            end
        end
        
        function set_element_gain(obj,power_gain_dB)
            % SET_ELEMENT_GAIN Sets the gain of each element in the array.
            % Does not account for element's radiation pattern; merely
            % scales the isotropic pattern. Can be used when considering
            % uniform gain over a range of directions (e.g., 3 dB gain from
            % -60 degrees to +60 degrees).
            %
            % Usage:
            %  SET_ELEMENT_GAIN(power_gain_dB)
            %
            % Args:
            %  power_gain_dB: the element's power gain in dB
            obj.element_gain_dB = power_gain_dB;
            obj.element_gain_linear = 10^(power_gain_dB/20);
        end
        
        function set_weights(obj,w)
            % SET_WEIGHTS Sets the complex weights of the array elements 
            % (analog beamforming). Note that the constraints of an array's
            % weights are hardware dependent. The TI IWR6843 does not 
            % support any analog beamforming on the receiver side and only
            % phase control on the transmitter side (with only two
            % transmit antennas active). The phase shifters have a
            % resolution of 6 bits.
            %
            % Usage:
            %  SET_WEIGHTS(w)
            % 
            % Args:
            %  w: an array weight vector where the ith array element is
            %  weighted by the ith element in w
            obj.weights = w(:); % Nt-by-1 column vector
        end
        
        function [x,th] = get_array_pattern_azimuth(obj,el)
            % GET_ARRAY_PATTERN_AZIMUTH Returns the transmit array pattern
            % in azimuth using the array weights.
            %
            % Usage:
            %  [x,th] = GET_ARRAY_PATTERN_AZIMUTH()
            %
            % Returns:
            %  x: the array pattern (with weights applied) as a function of
            %  azimuth angle (with an elevation of 0)
            %  th: the azimuth angles corresponding to the values in x
            th = -pi/2:pi/1024:pi/2;
            N = length(th);
            x = zeros(N,1);
            if nargin > 1
                ph = el;
            else
                ph = 0;
            end
            for i = 1:N
                x(i) = obj.weights.' * obj.get_array_response(th(i),ph);
            end
        end
        
        function [x,ph] = get_array_pattern_elevation(obj,az)
            % GET_ARRAY_PATTERN_ELEVATION Returns the transmit array 
            % pattern in elevation using the array weights.
            %
            % Usage:
            %  [x,ph] = GET_ARRAY_PATTERN_ELEVATION()
            %
            % Returns:
            %  x: the array pattern (with weights applied) as a function of
            %  elevation angle (with an azimuth of 0)
            %  ph: the elevation angles corresponding to the values in x
            ph = -pi/2:pi/1024:pi/2;
            N = length(ph);
            x = zeros(N,1);
            if nargin > 1
                th = az;
            else
                th = 0;
            end
            for i = 1:N
                x(i) = obj.weights.' * obj.get_array_response(th,ph(i));
            end
        end
        
        function x = get_array_response(obj,az,el)
            % GET_ARRAY_RESPONSE Returns the array response vector at a
            % given azimuth and elevation. This response is simply the
            % phase shifts experienced by the elements on an incoming plane
            % wave at a given azimuth and elevation.
            %
            % Usage:
            %  x = GET_ARRAY_RESPONSE(az)
            %  x = GET_ARRAY_RESPONSE(az,el)
            %
            % Args:
            %  az: azimuth angle to evaluate the array response
            %  el: elevation angle to evaluate the array response (default
            %  of 0)
            %
            % Returns:
            %  x: the array response at the azimuth and elevation of 
            %     interest
            if nargin < 3
                el = 0;
            end
            dx = obj.x - obj.x(1); % distances relative to first element
            dy = obj.y - obj.y(1); % distances relative to first element
            dz = obj.z - obj.z(1); % distances relative to first element
            x = exp(1j.*2*pi*(dx.*sin(az).*cos(el) + dy.*cos(az).*cos(el) + dz.*sin(el)));
            x = x(:); % return a column vector
            % x = x ./ sqrt(obj.Na);
        end
        
        function w = get_phased_array_beamformer(obj,az,el)
            % GET_PHASED_ARRAY_BEAMFORMER Returns the phased array
            % beamformer for beamsteering in a desired azimuth and
            % elevation.
            %
            % Usage:
            %  w = GET_PHASED_ARRAY_BEAMFORMER(az)
            %  w = GET_PHASED_ARRAY_BEAMFORMER(az,el)
            %
            % Args:
            %  az: azimuth angle to steer beamformer
            %  el: elevation angle to steer beamformer (optional)
            %
            % Returns:
            %  w: array weights corresponding to the phased array
            %     beamformer steered toward the azimuth and elevation of
            %     interest
            if nargin < 3
                el = 0;
            end
            w = conj(obj.get_array_response(az,el));
            % w = w ./ obj.Na;
            w = w(:);
        end
        
        function [A,th] = get_conjugate_beamformer_azimuth_matrix(obj,N,el)
            % GET_PHASED_ARRAY_BEAMFORMER_MATRIX Returns a matrix whose
            % columns are phased array beamformers in various directions of
            % azimuth.
            %
            % Usage:
            %  [A,th] = GET_PHASED_ARRAY_BEAMFORMER_AZIMUTH_MATRIX()
            %  [A,th] = GET_PHASED_ARRAY_BEAMFORMER_AZIMUTH_MATRIX(N)
            %  [A,th] = GET_PHASED_ARRAY_BEAMFORMER_AZIMUTH_MATRIX(N,el)
            %
            % Args:
            %  N: number of beamformers to return in matrix (number of
            %     columns in A)
            %  el: elevation angle used when giving the azimuth conjugate
            %      beamformer (optional, default of 0 radians)
            % 
            % Returns:
            %  A: matrix of size Na-by-N whose columns are the array 
            %     response vectors for the azimuth angles of interest at 
            %     the fixed elevation angle
            %  th: correpsonding azimuth angles which A is used for
            %      estimation
            if nargin < 2
                N = obj.Na;
            end
            if nargin < 3
                el = 0;
            end
            th = (0:N-1).*2*pi/N - pi; % azimuth angles of interest [-pi,+pi]
            A = zeros(obj.Na,N);
            for i = 1:N
                w = obj.get_array_response(th(i),el); % column vector
                A(:,i) = w;
            end
        end

        function gain = get_array_gain(obj,az,el)
            % GET_ARRAY_GAIN Returns the transmit array gain in a given
            % direction (azimuth and elevation) with the weights applied.
            %
            % Usage:
            %  gain = GET_ARRAY_GAIN(az)
            %  gain = GET_ARRAY_GAIN(az,el)
            %
            % Args:
            %  az: azimuth angle of interest (radians)
            %  el: elevation angle of interest (radians) (optional)
            %
            % Returns:
            %  gain: complex gain in the azimuth and elevation of interest
            if nargin < 3
                el = 0;
            end
            gain = obj.weights.' * obj.get_array_response(az,el);
        end
        
        function show_beamformer_pattern(obj,w)
            % SHOW_BEAMFORMER_PATTERN Plots the resulting beam pattern when
            % the array employs a specific set of beamforming weights.
            %
            % Usage:
            %  SHOW_BEAMFORMER_PATTERN(w)
            %
            % Args:
            %  w: a vector of complex beamforming weights
            % 
            % Notes:
            %  This temporarily sets the array weights so that we can
            %  utilize the already-written function
            %  array.show_array_pattern(). The original weights are then
            %  reapplied at the end of the function. In sum, the original
            %  array weights will remain.
            w = w(:);
            if length(w) ~= obj.Na
                error('Beamforming vector must have the same number of entries as the number of array elements.');
            end
            orig_weights = obj.weights;
            obj.set_weights(w);
            obj.show_array_pattern();
            obj.set_weights(orig_weights);
        end
        
        function show_array_pattern(obj,az,el)
            % SHOW_ARRAY_PATTERN Plots the array pattern as a function of
            % azimuth and elevation.
            %
            % Usage:
            %  SHOW_ARRAY_PATTERN()
            if nargin < 3
                el = 0;
            end
            if nargin < 2
                az = 0;
            end
            [xx,th] = obj.get_array_pattern_azimuth(el);
            figure();
            subplot(221);
            plot(th/pi*180,abs(xx),'-k'); grid on;
            xlabel('Azimuth (deg)');
            ylabel('Linear magnitude');
            subplot(223);
            plot(th/pi*180,angle(xx),'-k'); grid on;
            xlabel('Azimuth (deg)');
            ylabel('Phase (rad)');
            [xx,ph] = obj.get_array_pattern_elevation(az);
            subplot(222);
            plot(ph/pi*180,abs(xx),'-k'); grid on;
            xlabel('Elevation (deg)');
            ylabel('Linear magnitude');
            subplot(224);
            plot(ph/pi*180,angle(xx),'-k'); grid on;
            xlabel('Elevation (deg)');
            ylabel('Phase (rad)');
        end
        
        function [xx,th] = get_element_pattern_azimuth(obj)
            % GET_ELEMENT_PATTERN_AZIMUTH Returns the element pattern as
            % a function of azimuth.
            % 
            % Usage:
            %  [xx,th] = GET_ELEMENT_PATTERN_AZIMUTH()
            %
            % Returns:
            %  xx: the element pattern as a function of azimuth angle
            %  th: azimuth angles corresponding to the values in xx
            %  (radians)
            xx = obj.ep;
            th = 1;
        end
        
        function [xx,ph] = get_element_pattern_elevation(obj)
            % GET_ELEMENT_PATTERN_ELEVATION Returns the element pattern as
            % a function of elevation.
            %
            % Usage:
            %  [xx,ph] = GET_ELEMENT_PATTERN_ELEVATION()
            %
            % Returns:
            %  xx: the element pattern as a function of elevation angle
            %  ph: elevation angles corresponding to the values in xx
            %  (radians)
            xx = obj.ep;
            ph = 1;
        end
        
        function show_array_pattern_azimuth(obj,ax)
            % SHOW_ARRAY_PATTERN_AZIMUTH Plots the azimuth array pattern in
            % a standard x-y axis fashion.
            %
            % Usage:
            %  SHOW_ARRAY_PATTERN_AZIMUTH()
            %  SHOW_ARRAY_PATTERN_AZIMUTh(ax)
            %
            % Args:
            %  ax: an existing figure axis
            % 
            % Notes:
            %  This function was originally created for plotting in a GUI.
            if nargin < 2 || isempty(ax)
                figure();
                ax = axes();
            end
            [xx,th] = obj.get_array_pattern_azimuth();
            plot(ax,th*180/pi,abs(xx),'-k'); 
            grid(ax,'on');
            xlabel(ax,'Azimuth (degrees)');
            ylabel(ax,'Magnitude (linear)');
            title(ax,'Azimuth pattern');
        end
        
        function show_array_pattern_elevation(obj,ax)
            % SHOW_ARRAY_PATTERN_ELEVATION Plots the elevation array 
            % pattern in a standard x-y axis fashion.
            %
            % Usage:
            %  SHOW_ARRAY_PATTERN_ELEVATION()
            %  SHOW_ARRAY_PATTERN_ELEVATION(ax)
            %
            % Args:
            %  ax: an existing figure axis
            % 
            % Notes:
            %  This function was originally created for plotting in a GUI.
            if nargin < 2 || isempty(ax)
                figure();
                ax = axes();
            end
            [xx,th] = obj.get_array_pattern_elevation();
            plot(ax,th*180/pi,abs(xx),'-k'); 
            grid(ax,'on');
            xlabel(ax,'Elevation (degrees)');
            ylabel(ax,'Magnitude (linear)');
            title(ax,'Elevation pattern');
        end
        
        function show_element_pattern(obj)
            % SHOW_ELEMENT_PATTERN Plots the element pattern as a function 
            % of azimuth and elevation.
            % 
            % Usage:
            %  SHOW_ELEMENT_PATTERN()
            [xx,th] = obj.get_element_pattern_azimuth();
            figure();
            subplot(221);
            plot(th,abs(xx),'-k'); grid on;
            xlabel('Azimuth (rad)');
            ylabel('Linear magnitude');
            subplot(223);
            plot(th,angle(xx),'-k'); grid on;
            xlabel('Azimuth (rad)');
            ylabel('Phase (rad)');
            [xx,th] = obj.get_element_pattern_azimuth();
            subplot(222);
            plot(th,abs(xx),'-k'); grid on;
            xlabel('Elevation (rad)');
            ylabel('Linear magnitude');
            subplot(224);
            plot(th,angle(xx),'-k'); grid on;
            xlabel('Elevation (rad)');
            ylabel('Phase (rad)');
        end
        
        function w = get_zero_forcing_beamformer(obj,az_des,el_des,az_int,el_int)
            % GET_ZERO_FORCING_BEAMFORMER Returns the zero-forcing
            % beamformer that accepts energy in one or more desired
            % directions and rejects energy in one or more interfering
            % directions.
            %
            % Usage:
            %  w = GET_ZERO_FORCING_BEAMFORMER(az_des,el_des,az_int,el_int)
            %
            % Args:
            %  az_des: desired azimuth angle(s) in radians
            %  el_des: desired elevation angles(s) in radians
            %  az_int: interfering azimuth angle(s) in radians
            %  el_int: interfering elevation angle(s) in radians
            %
            % Returns:
            %  w: a beamformer that attempts to accept energy in the
            %     desired directions while rejecting energy in the
            %     interfering directions
            %
            % Reference:
            %  Equation (9.8) in Heath and Lozano.
            num_des = length(az_des); % number of desired directions
            num_int = length(az_int); % number of interferer directions
            H_des = zeros(size(obj.get_array_response(0,0)));
            for i = 1:num_des
                h = obj.get_array_response(az_des(i),el_des(i));
                H_des = H_des + h; % effective desired channel
            end
            H_int = zeros(size(obj.get_array_response(0,0)));
            for i = 1:num_int
                h = obj.get_array_response(az_int(i),el_int(i));
                H_int = H_int + h; % effective interfering channel
            end
            w = pinv([H_des(:), H_int(:)]);
            w = w(1,:).'; % 1 is the index of the desired
            w = obj.Na .* w; % just so it is same gain as conjugate BF
        end
        
        function show_polar_array_pattern_azimuth(obj,ax)
            % SHOW_POLAR_ARRAY_PATTERN_AZIMUTH Plots the azimuth array
            % pattern in a polar plot.
            %
            % Usage:
            %  SHOW_POLAR_ARRAY_PATTERN_AZIMUTH()
            if nargin < 2 || isempty(ax)
                figure();
                ax = polaraxes();
            end
            [x,th] = obj.get_array_pattern_azimuth();
            polarplot(ax,th.',abs(x),'-k'); 
            thetalim([min(th)*180/pi max(th)*180/pi]);
            % rlim([0 obj.Na]);
            % rlim([0 sqrt(obj.Na)]);
            ax = gca;
            ax.ThetaDir = 'clockwise';
            ax.ThetaZeroLocation = 'top';
            ax.RAxis.Label.String = 'Magnitude (linear)';
        end
        
        function show_polar_array_pattern_elevation(obj,ax)
            % SHOW_POLAR_ARRAY_PATTERN_ELEVATION Plots the azimuth array
            % pattern in a polar plot.
            %
            % Usage:
            %  SHOW_POLAR_ARRAY_PATTERN_ELEVATION()
            if nargin < 2 || isempty(ax)
                figure();
                ax = polaraxes();
            end
            [x,ph] = obj.get_array_pattern_elevation();
            polarplot(ax,ph.',abs(x),'-k'); 
            % thetalim(ax,[min(ph)*180/pi max(ph)*180/pi]);
            ax.ThetaDir = 'clockwise';
            ax.ThetaZeroLocation = 'top';
            ax.RAxis.Label.String = 'Magnitude (linear)';
        end
        
        function show2d(obj,ax)
            % SHOW2D Plots the array elements in 2-D space for your viewing
            % pleasure (x-y plane).
            % 
            % Usage:
            %  SHOW2D()
            if nargin < 2 || isempty(ax)
                figure();
                ax = axes();
            end
            for i = 1:obj.Na
                plot(ax,obj.x(i),obj.y(i),obj.marker); 
                hold(ax,'on');
            end
            hold(ax,'off');
            grid(ax,'on');
            xlabel(ax,'$x$ (in $\lambda$)');
            ylabel(ax,'$y$ (in $\lambda$)');
        end
        
        function show3d(obj,ax)
            % SHOW3D Plots the array elements in 3-D space for your viewing
            % pleasure.
            %
            % Usage:
            %  SHOW3D()
            if nargin < 2 || isempty(ax)
                figure();
                ax = axes();
            end
            for i = 1:obj.Na
                scatter3(ax,obj.x(i),obj.y(i),obj.z(i),obj.marker);
                hold(ax,'on');
            end
            hold(ax,'off');
            grid(ax,'on');
            xlabel(ax,'$x$ (in $\lambda$)');
            ylabel(ax,'$y$ (in $\lambda$)');
            zlabel(ax,'$z$ (in $\lambda$)');
            % title(ax,'Array elements');
        end
    end
end