classdef channel < handle
    % CHANNEL A MIMO channel.
    properties
        name; % human-readable identifier
        H; % Nr-by-Nt channel matrix
        Nt; % number of transmit antennas
        Nr; % number of receive antennas
        atx; % transmit array object
        arx; % receive array object
        carrier_frequency; % carrier frequency (Hz)
        carrier_wavelength; % carrier wavelength (m)
        propagation_velocity; % propagation velocity (m/s)
    end
    methods
        function obj = channel(name)
            % CHANNEL Creates a MIMO channel object.
            % 
            % Usage:
            %  obj = CHANNEL()
            %  obj = CHANNEL(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing a MIMO channel
            if nargin < 1 || isempty(name)
                name = 'channel';
            end
            obj.Nt = 1;
            obj.Nr = 1;
            obj.carrier_frequency = 1;
            obj.carrier_wavelength = 1;
            obj.propagation_velocity = 1;
            obj.name = name;
        end
        
        function set_propagation_velocity(obj,val)
            % SET_PROPAGATION_VELOCITY Sets the propagation velocity of the
            % channel.
            %
            % Usage:
            %  SET_PROPAGATION_VELOCITY(val)
            %
            % Args:
            %  val: propagation velocity (meters/sec)
            obj.propagation_velocity = val;
            obj.carrier_wavelength = val / obj.carrier_frequency;
        end
        
        function set_carrier_frequency(obj,fc)
            % SET_CARRIER_FREQUENCY Sets the carrier frequency of the
            % channel.
            %
            % Usage:
            %  SET_CARRIER_FREQUENCY(fc)
            %
            % Args:
            %  fc: carrier frequency (Hz)
            obj.carrier_frequency = fc;
            obj.carrier_wavelength = obj.propagation_velocity / fc;
        end
        
        function set_transmit_array(obj,array)
            % SET_TRANSMIT_ARRAY Sets the transmit array object. Also sets
            % the number of transmit antennas accordingly.
            % 
            % Usage:
            %  SET_TRANSMIT_ARRAY(array)
            % 
            % Args:
            %  array: an array object
            obj.atx = array;
            obj.Nt = array.Na;
        end
        
        function set_receive_array(obj,array)
            % SET_RECEIVE_ARRAY Sets the receive array object. Also sets
            % the number of receive antennas accordingly.
            % 
            % Usage:
            %  SET_RECEIVE_ARRAY(array)
            % 
            % Args:
            %  array: an array object
            obj.arx = array;
            obj.Nr = array.Na;
        end
        
        function set_channel_matrix(obj,H)
            % SET_CHANNEL_MATRIX Sets the channel matrix.
            %
            % Usage:
            %  SET_CHANNEL_MATRIX(H)
            % 
            % Args:
            %  H: channel matrix
            obj.H = H;
        end
        
        function H = get_channel_matrix(obj)
            % GET_CHANNEL_MATRIX Returns the channel matrix.
            %
            % Usage:
            %  H = GET_CHANNEL_MATRIX()
            % 
            % Returns:
            %  H: channel matrix
            if ~isempty(obj.H)
                H = obj.H;
            else
                H = zeros(obj.Nr,obj.Nt); % so that default is there being no channel
            end
        end
    end
end