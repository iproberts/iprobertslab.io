classdef channel_mimo_ray_cluster < channel
    % CHANNEL_MIMO_RAY_CLUSTER A MIMO channel comprised of a sum of rays.
    properties
        num_clusters; % number of lcusters
        num_clusters_range; % range of the number of clusters
        num_rays_per_cluster; % number of rays per cluster
        num_rays_per_cluster_range; % range of the number of rays per cluster
        cluster_AoD; % cluster AoD (azimuth and elevation) (radians)
        cluster_AoA; % cluster AoA (azimuth and elevation) (radians)
        AoD; % ray angles of departure (azimuth and elevation) (radians)
        AoA; % ray angles of arrival (azimuth and elevation) (radians)
        AoD_range; % the range for AoDs (azimuth and elevation) (radians)
        AoA_range; % the range for AoAs (azimuth and elevation) (radians)
        cluster_angle_distribution; % distribution of cluster AoDs and AoAs
        ray_angle_distribution; % distribution of ray's angles around its cluster's mean
        ray_gain_distribution; % distribution of ray gain
        ray_gains; % complex gain of each ray in each cluster
    end
    methods
        function obj = channel_mimo_ray_cluster(name)
            % CHANNEL_MIMO_RAY_CLUSTER Creates a MIMO channel object based
            % on the ray/cluster model.
            %
            % Usage:
            %  obj = CHANNEL_MIMO_RAY_CLUSTER()
            %  obj = CHANNEL_MIMO_RAY_CLUSTER(name)
            %
            % Args:
            %  name: an optional name for the object
            %
            % Returns:
            %  obj: an object representing a MIMO channel based on the
            %       ray/cluster model
            if nargin < 1 || isempty(name)
                name = 'mimo-ray-cluster-channel';
            end
            obj.name = name;
            obj.carrier_frequency = 1;
            obj.carrier_wavelength = 1;
            obj.propagation_velocity = 1;
            obj.AoD_range = zeros(2,2); % init
            obj.AoA_range = zeros(2,2); % init
            obj.AoD_range(1,:) = [-pi/2, pi/2]; % azimuth
            obj.AoD_range(2,:) = [0, 0]; % elevation
            obj.AoA_range(1,:) = [-pi/2, pi/2]; % azimuth
            obj.AoA_range(2,:) = [0, 0]; % elevation
            obj.ray_angle_distribution.type = 'uniform';
            obj.ray_gain_distribution.type = 'cgauss';
        end
        
        function set_ray_gain_distribution(obj,opt)
            % SET_RAY_GAIN_DISTRIBUTION Sets the distibution of the ray
            % gains.
            %
            % Usage:
            %  SET_RAY_GAIN_DISTRIBUTION(opt)
            %
            % Args:
            %  opt: a struct specifying the distribution; opt.type should
            %  specify the distribution (e.g., 'unit' or 'cgauss'); other
            %  optional distribution parameters (e.g., variance) could be
            %  specified in the struct, though not currently supported
            obj.ray_gain_distribution = opt;
        end
        
        function AoD_AoA_range_check(obj,rng)
            % AOD_AOA_RANGE_CHECK Checks a range matrix to ensure it is
            % proper for setting the AoA and AoD ranges.
            %
            % Usage:
            %  AOD_AOA_RANGE_CHECK(rng)
            %
            % Args:
            %  rng: a 2-by-2 matrix where the first row is a vector of
            %       azimuth ranges (in radians) and the second row is a 
            %       vector of elevation ranges (in radians)
            % 
            % See also: 
            %  CHANNEL_MIMO_RAY_CLUSTER.SET_AOD_RANGE
            %  CHANNEL_MIMO_RAY_CLUSTER.SET_AOA_RANGE
            [M,N] = size(rng);
            if ~(M == 2 && N == 2)
                error('Range matrix must be of size 2 x 2.');
            end
            if rng(1,1) > rng(1,2) || rng(2,1) > rng(2,2)
                error('Range values must be increasing across rows.');
            end
        end
        
        function set_AoA_range(obj,rng)
            % SET_AOA_RANGE Sets the range of possible angles of arrival
            % (AoA) in radians.
            % 
            % Usage:
            %  SET_AOA_RANGE(rng)
            %
            % Args:
            %  rng: a 2-by-2 matrix where the first row is a vector of
            %       azimuth ranges (in radians) and the second row is a 
            %       vector of elevation ranges (in radians)
            %
            % See also: 
            %  CHANNEL_MIMO_RAY_CLUSTER.AOD_AOA_RANGE_CHECK
            obj.AoD_AoA_range_check(rng);
            obj.AoA_range = rng;
        end
        
        function set_AoD_range(obj,rng)
            % SET_AOD_RANGE Sets the range of possible angles of departure
            % (AoD) in radians.
            % 
            % Usage:
            %  SET_AOD_RANGE(rng)
            %
            % Args:
            %  rng: a 2-by-2 matrix where the first row is a vector of
            %       azimuth ranges (in radians) and the second row is a 
            %       vector of elevation ranges (in radians)
            %
            % See also: 
            %  CHANNEL_MIMO_RAY_CLUSTER.AOD_AOA_RANGE_CHECK
            obj.AoD_AoA_range_check(rng);
            obj.AoD_range = rng;
        end
        
        function set_ray_angle_distribution(obj,opt)
            % SET_RAY_ANGLE_DISTRIBUTION Sets the distibution of the ray
            % angles of departure and arrival.
            %
            % Usage:
            %  SET_RAY_ANGLE_DISTRIBUTION(opt)
            %
            % Args:
            %  opt: a struct specifying the distribution; opt.type should
            %  specify the distribution (e.g., 'unit' or 'cgauss'); other
            %  optional distribution parameters (e.g., variance) could be
            %  specified in the struct, though not currently supported
            obj.ray_angle_distribution = opt;
        end
               
        function set_num_clusters_range(obj,M,N)
            % SET_NUM_CLUSTERS Sets the number of clusters in the channel.
            %
            % Usage:
            %  SET_NUM_CLUSTERS(N)
            % 
            % Args:
            %  N: either an integer or a 1-by-2 vector of integers
            %     representing the number of clusters
            %
            % Notes:
            %  If N = a, the number of clusters in the channel is fixed and
            %  equal to a. 
            %
            %  If N = [a b] or N = [a; b], the number of clusters in the
            %  channel is drawn uniformly from the range [a,b] (inclusive)
            %  during each channel realization.
            if nargin < 3 || isempty(N)
                N = M;
            end
            obj.num_clusters_range = [M,N];
        end
        
        function set_num_rays_per_cluster_range(obj,M,N)
            % SET_NUM_RAYS_PER_CLUSTER_RANGE Sets the number of rays per 
            % cluster in the channel.
            %
            % Usage:
            %  SET_NUM_RAYS_PER_CLUSTER_RANGE(M)
            %  SET_NUM_RAYS_PER_CLUSTER_RANGE(M,N)
            % 
            % Args:
            %
            % Notes:
            %  If N = a, the number of rays per cluster is fixed and equal 
            %  to a. 
            %
            %  If N = [a b] or N = [a; b], the number of rays per cluster
            %  is drawn uniformly from the range [a,b] (inclusive) during 
            %  each channel realization.
            if nargin < 3 || isempty(N)
                N = M;
            end
            obj.num_rays_per_cluster_range = [M,N];
        end
        
        function generate_num_clusters(obj)
            % GENERATE_NUM_CLUSTERS Generates the number of clusters in the
            % channel.
            %
            % Usage:
            %  GENERATE_NUM_CLUSTERS()
            % 
            % Notes: 
            %  obj.num_clusters is updated with a realization of the number
            %  of clusters in the channel.
            lower = obj.num_clusters_range(1);
            upper = obj.num_clusters_range(2);
            obj.num_clusters = round(uniform_rv(lower,upper)); % !!! not perfectly uniform !!!
        end
        
        function generate_num_rays_per_cluster(obj)
            % GENERATE_NUM_RAYS_PER_CLUSTER Generates the number of rays 
            % per cluster in the channel.
            %
            % Usage:
            %  GENERATE_NUM_RAYS_PER_CLUSTER()
            % 
            % Notes: 
            %  obj.num_rays_per_cluster is updated with a realization of 
            %  the number of rays per cluster in the channel.
            lower = obj.num_rays_per_cluster_range(1);
            upper = obj.num_rays_per_cluster_range(2);
            obj.num_rays_per_cluster = round(uniform_rv(lower,upper)); % !!! not perfectly uniform !!!
        end
        
        function generate_cluster_AoD(obj)
            % GENERATE_CLUSTER_AOD Generates the clusters' angles of 
            % departure.
            %
            % Usage:
            %  GENERATE_CLUSTER_AoD()
            % 
            % Notes:
            %  obj.cluster_AoD is updated which are the
            %  mean angles for the rays coming from their respective
            %  clusters.
            obj.cluster_AoD = [];
            
            % azimumth
            lower = obj.AoD_range(1,1);
            upper = obj.AoD_range(1,2);
            obj.cluster_AoD(:,1) = uniform_rv(lower,upper,obj.num_clusters,1);
            
            % elevation
            lower = obj.AoD_range(2,1);
            upper = obj.AoD_range(2,2);
            obj.cluster_AoD(:,2) = uniform_rv(lower,upper,obj.num_clusters,1);
        end
        
        function generate_cluster_AoA(obj)
            % GENERATE_CLUSTER_AOA Generates the clusters'
            % angles of arrival.
            %
            % Usage:
            %  GENERATE_CLUSTER_AOA()
            % 
            % Notes:
            %  obj.cluster_AoA is updated which are the
            %  mean angles for the rays coming from their respective
            %  clusters.
            if isempty(obj.num_clusters)
                error('Number of clusters not yet set.');
            end
            
            obj.cluster_AoA = [];

            % azimuth
            lower = obj.AoA_range(1,1);
            upper = obj.AoA_range(1,2);
            obj.cluster_AoA(:,1) = uniform_rv(lower,upper,obj.num_clusters,1);
            
            % elevation
            lower = obj.AoA_range(2,1);
            upper = obj.AoA_range(2,2);
            obj.cluster_AoA(:,2) = uniform_rv(lower,upper,obj.num_clusters,1);
        end
        
        function generate_ray_AoD(obj)
            % GENERATE_RAY_AOD Generates the angles of
            % departure for each ray with each cluster of the channel.
            %
            % Usage:
            %  GENERATE_RAY_AOD()
            
            % for each cluster,ray pair there is an azimuth and elevation
            obj.AoD = zeros(obj.num_clusters,obj.num_rays_per_cluster,2);
            for i = 1:obj.num_clusters
                for j = 1:obj.num_rays_per_cluster
                    if strcmp(obj.ray_angle_distribution.type,'laplacian')
                        mu = obj.cluster_AoD(i,1);
                        sigma = obj.ray_angle_distribution.std_dev(1);
                        obj.AoD(i,j,1) = laplace_rv(mu,sigma,1,1);
                        mu = obj.cluster_AoD(i,2);
                        sigma = obj.ray_angle_distribution.std_dev(2);
                        obj.AoD(i,j,2) = laplace_rv(mu,sigma,1,1);
                    end
                end
            end
            
            % azimuth
            lower = obj.AoD_range(1,1);
            upper = obj.AoD_range(1,2);
            obj.AoD(:,:,1) = constrain(obj.AoD(:,:,1),lower,upper);
            
            % elevation
            lower = obj.AoD_range(2,1);
            upper = obj.AoD_range(2,2);
            obj.AoD(:,:,2) = constrain(obj.AoD(:,:,2),lower,upper);
        end
        
        function generate_ray_AoA(obj)
            % GENERATE_RAY_AOA Generates the angles of
            % arrival for each ray within each cluster of the channel.
            %
            % Usage:
            %  GENERATE_RAY_ANGLES_OF_ARRIVAL()
            
            % for each cluster,ray pair there is an azimuth and elevation
            obj.AoA = zeros(obj.num_clusters,obj.num_rays_per_cluster,2);
            for i = 1:obj.num_clusters
                for j = 1:obj.num_rays_per_cluster
                    if strcmp(obj.ray_angle_distribution.type,'laplacian')
                        mu = obj.cluster_AoA(i,1);
                        sigma = obj.ray_angle_distribution.std_dev(1);
                        obj.AoA(i,j,1) = laplace_rv(mu,sigma,1,1); % az
                        mu = obj.cluster_AoA(i,2);
                        sigma = obj.ray_angle_distribution.std_dev(2);
                        obj.AoA(i,j,2) = laplace_rv(mu,sigma,1,1); % el
                    end
                end
            end
            
            % azimuth
            lower = obj.AoA_range(1,1);
            upper = obj.AoA_range(1,2);
            obj.AoA(:,:,1) = constrain(obj.AoA(:,:,1),lower,upper);
            
            % elevation
            lower = obj.AoA_range(2,1);
            upper = obj.AoA_range(2,2);
            obj.AoA(:,:,2) = constrain(obj.AoA(:,:,2),lower,upper);
        end
        
        function generate_ray_gains(obj)
            % GENERATE_RAY_GAINS Generates the complex gain of each ray in
            % the channel (drawn from circularly symmetric Normal dist.).
            %
            % Usage:
            %  GENERATE_RAY_GAINS()
            opt = obj.ray_gain_distribution.type;
            M = obj.num_clusters;
            N = obj.num_rays_per_cluster;
            if strcmp(opt,'cgauss')
                g = cgauss_rv(0,1,M,N);
            elseif strcmp(opt,'cgauss_exp')
                fc = 60e9; % carrier freq (Hz)
                max_dist = 100; % meters
                d = rand([M,N]).*max_dist;
                tau = d ./ 3e8;
                g = cgauss_rv(0,1,M,N) .* exp(-1j.*2.*pi.*fc.*tau);
            elseif strcmp(opt,'unit')
                g = ones(M,N);
            end
            obj.ray_gains = g;
        end
        
        function show_beamspace_azimuth(obj)
            % SHOW_BEAMSPACE_AZIMUTH Plots the ray gains in a scatter plot
            % as a function of azimuth AoD and azimuth AoA.
            %
            % Usage:
            %  SHOW_BEAMSPACE_AZIMUTH()
            A = obj.AoA(:,:,1);
            D = obj.AoD(:,:,1);
            A = A(:) * 180 / pi;
            D = D(:) * 180 / pi;
            intensity = abs(obj.ray_gains(:));
            figure();
            scatter(D,A,[],intensity,'filled'); grid on;
            xlabel('Angle of Departure (degrees)');
            ylabel('Angle of Arrival (degrees)');
            title('Azimuth');
            xlim(180./pi.*obj.AoD_range(1,:).');
            ylim(180./pi.*obj.AoA_range(1,:).');
        end
        
        function show_beamspace_elevation(obj)
            % SHOW_BEAMSPACE_ELEVATION Plots the ray gains in a scatter 
            % plot as a function of elevation AoD and elevation AoA.
            %
            % Usage:
            %  SHOW_BEAMSPACE_ELEVATION()
            A = obj.AoA(:,:,2);
            D = obj.AoD(:,:,2);
            A = A(:) * 180 / pi;
            D = D(:) * 180 / pi;
            intensity = abs(obj.ray_gains(:));
            figure();
            scatter(D,A,[],intensity,'filled'); grid on;
            xlabel('Angle of Departure (degrees)');
            ylabel('Angle of Arrival (degrees)');
            title('Elevation');
            xlim(180./pi.*obj.AoD_range(2,:).');
            ylim(180./pi.*obj.AoA_range(2,:).');
        end
        
        function generate_channel_matrix(obj)
            % GENERATE_CHANNEL_MATRIX Generates the channel matrix based on
            % the associated random variables and the array response
            % vectors of the transmit and receive arrays.
            %
            % Usage:
            %  GENERATE_CHANNEL_MATRIX()
            T = zeros(obj.Nr,obj.Nt);
            D = obj.AoD(:,:,1); % az
            D_az = D(:);
            D = obj.AoD(:,:,2); % el
            D_el = D(:);
            D = [D_az D_el];
            
            A = obj.AoA(:,:,1); % az
            A_az = A(:);
            A = obj.AoA(:,:,2); % el
            A_el = A(:);
            A = [A_az A_el];
            
            G = obj.ray_gains(:);
            for i = 1:length(G)
                AoD_az = D(i,1);
                AoD_el = D(i,2);
                AoA_az = A(i,1);
                AoA_el = A(i,2);
                T = T + G(i) * obj.arx.get_array_response(AoA_az,AoA_el) * obj.atx.get_array_response(AoD_az,AoD_el)';
            end
            T = T ./ obj.Nt;
            Nt = obj.Nt;
            Nr = obj.Nr;
            N_clust = obj.num_clusters;
            N_rays = obj.num_rays_per_cluster;
            T = T * sqrt(Nt * Nr / N_clust / N_rays);
            obj.set_channel_matrix(T);
        end
        
        function H = channel_realization(obj)
            % CHANNEL_REALIZATION Realizes a random instance of the channel
            % matrix based on the channel object's parameters.
            %
            % Usage:
            %  H = CHANNEL_REALIZATION()
            %
            % Returns:
            %  H: the resulting channel matrix
            obj.generate_num_clusters();
            obj.generate_num_rays_per_cluster();
            obj.generate_cluster_AoD();
            obj.generate_ray_AoD();
            obj.generate_cluster_AoA();
            obj.generate_ray_AoA();
            obj.generate_ray_gains();
            obj.generate_channel_matrix();
            H = obj.get_channel_matrix();
        end
    end
end