classdef channel_mimo_ray_cluster_frequency_selective < channel_mimo_ray_cluster
    properties
        num_taps;
        cluster_delays;
        ray_delays;
        ts;
        fs;
        pulse;
        ray_pulse_shape_gains;
        channel_matrix_sequence; % (tap,rx,tx)
    end
    methods
        function obj = channel_mimo_ray_cluster_frequency_selective(name)
            if nargin < 1 || isempty(name)
                name = 'channel-mimo-ray-cluster-frequency-selective';
            end
            obj.name = name;
        end
        
        function set_pulse(obj,pulse)
            obj.pulse = pulse;
        end
        
        function set_sampling_rate(obj,fs)
            obj.fs = fs;
            obj.ts = 1 / fs;
        end
        
        function set_sampling_period(obj,ts)
            obj.ts = ts;
            obj.fs = 1 / ts;
        end
        
        function set_num_taps(obj,D)
            obj.num_taps = D;
        end
        
        function generate_cluster_delays(obj)
            obj.cluster_delays = zeros(obj.num_taps,obj.num_clusters);
            for i = 1:obj.num_taps
                min_cluster_delay_sec = (i-1) .* obj.ts;
                max_cluster_delay_sec = (i) .* obj.ts;
                for j = 1:obj.num_clusters
                    obj.cluster_delays(i,j) = uniform_rv(min_cluster_delay_sec,max_cluster_delay_sec);
                end
            end
        end
        
        function generate_ray_delays(obj)
            obj.ray_delays = zeros(obj.num_taps,obj.num_clusters,obj.num_rays_per_cluster);
            for k = 1:obj.num_taps
                for i = 1:obj.num_clusters
                    min_ray_delay_sec = obj.cluster_delays(i);
                    max_ray_delay_sec = min_ray_delay_sec + obj.ts; % !!! issue here with exceeding number of taps? !!!
                    for j = 1:obj.num_rays_per_cluster
                        obj.ray_delays(k,i,j) = uniform_rv(min_ray_delay_sec,max_ray_delay_sec);
                    end
                end
            end
        end
        
        function generate_ray_pulse_shape_gains(obj)
            obj.ray_pulse_shape_gains = zeros(obj.num_taps,obj.num_clusters,obj.num_rays_per_cluster);
            for k = 1:obj.num_taps
                for i = 1:obj.num_clusters
                    for j = 1:obj.num_rays_per_cluster
                        tau = obj.ray_delays(k,i,j);
                        obj.ray_pulse_shape_gains(k,i,j) = obj.get_pulse_shape_gain(tau);
                    end
                end
            end
        end
        
        function val = get_pulse_shape_gain(obj,tau)
            val = obj.get_raised_cosine_pulse_shape_gain(tau);
        end
        
        function val = get_raised_cosine_pulse_shape_gain(obj,t)
            beta = obj.pulse.beta;
            Ts = obj.ts;
            if abs(t) == Ts / (2 * beta)
                val = pi/4 * sinc(1/(2*beta));
            else
                val = sinc(t/Ts) * (cos(pi*beta*t/Ts)) / (1 - (2*beta*t/Ts)^2);
            end
        end
        
        function generate_channel_matrix_sequence(obj)
            H = zeros(obj.num_taps,obj.Nr,obj.Nt);
            for d = 1:obj.num_taps
                obj.generate_cluster_AoD();
                obj.generate_ray_AoD();
                obj.generate_cluster_AoA();
                obj.generate_ray_AoA();
                obj.generate_ray_gains();
                H(d,:,:) = obj.generate_channel_matrix(d);
            end
            obj.channel_matrix_sequence = H;
        end
        
        function T = generate_channel_matrix(obj,tap)
            % GENERATE_CHANNEL_MATRIX Generates the channel matrix based on
            % the associated random variables and the array response
            % vectors of the transmit and receive arrays.
            %
            % Usage:
            %  GENERATE_CHANNEL_MATRIX()
            T = zeros(obj.Nr,obj.Nt);
            D = obj.AoD(:,:,1); % az
            D_az = D(:);
            D = obj.AoD(:,:,2); % el
            D_el = D(:);
            D = [D_az D_el];
            
            A = obj.AoA(:,:,1); % az
            A_az = A(:);
            A = obj.AoA(:,:,2); % el
            A_el = A(:);
            A = [A_az A_el];
            
            G = obj.ray_gains(:,:);
            P = obj.ray_pulse_shape_gains(tap,:,:);
            G = G(:);
            P = P(:);
            for i = 1:length(G)
                AoD_az = D(i,1);
                AoD_el = D(i,2);
                AoA_az = A(i,1);
                AoA_el = A(i,2);
                T = T + G(i) * P(i) * obj.arx.get_array_response(AoA_az,AoA_el) * obj.atx.get_array_response(AoD_az,AoD_el)';
            end
            T = T ./ obj.Nt;
            Nt = obj.Nt;
            Nr = obj.Nr;
            N_clust = obj.num_clusters;
            N_rays = obj.num_rays_per_cluster;
            T = T * sqrt(Nt * Nr / N_clust / N_rays);
            % obj.set_channel_matrix(T);
            % norm(T,'fro').^2
        end
             
        function H = channel_realization(obj)
            % CHANNEL_REALIZATION Realizes a random instance of the channel
            % matrix based on the channel object's parameters.
            %
            % Usage:
            %  H = CHANNEL_REALIZATION()
            %
            % Returns:
            %  H: the resulting channel matrix
            obj.generate_num_clusters(); % num clusters fixed for each tap
            obj.generate_num_rays_per_cluster(); % num rays fixed for each tap
            obj.generate_cluster_delays();
            obj.generate_ray_delays();
            obj.generate_ray_pulse_shape_gains();
            obj.generate_channel_matrix_sequence();
            H = obj.get_channel_matrix_sequence();
        end
        
        function H = get_channel_matrix_sequence(obj)
            H = obj.channel_matrix_sequence;
        end
        
        function T = fft_channel_matrix_sequence(obj,K)
            [L,M,N] = size(obj.channel_matrix_sequence);
            if nargin < 2 || isempty(K)
                % K = 2^nextpow2(L);
                K = L;
            end
            T = zeros(L,M,N);
            for k = 1:K
                for d = 1:L
                    H = obj.channel_matrix_sequence(d,:,:);
                    T(k,:,:) = T(k,:,:) + H .* exp(-1j*2*pi*k*d/K);
                end
            end
        end
        
        function show_pulse_shape(obj)
            t = ((0:0.01:20) - 10) .* obj.ts;
            val = zeros(length(t),1);
            for i = 1:length(t)
                val(i) = obj.get_pulse_shape_gain(t(i));
            end
            figure();
            plot(t,val,'-k');
            grid on;
            xlabel('Time (sec)');
            ylabel('Amplitude');
        end                
    end
end
