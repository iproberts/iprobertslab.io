classdef channel_self_interference_frequency_selective < channel_self_interference
   methods
       function H = rician_add(obj,LOS,NLOS,K)
            % RICIAN_ADD Adds a line-of-sight MIMO channel and non-line-of-
            % sight MIMO channel in a Rician fashion.
            %
            % Usage:
            %  H = RICIAN_ADD(LOS,NLOS,K)
            %
            % Args:
            %  LOS: line-of-sight MIMO channel
            %  NLOS: non-line-of-sight MIMO channel
            %  K: the Rician factor (linear power)
            %
            % Returns:
            %  H: the resulting channel matrix after Rician summation
            %
            % Notes:
            %  Notice that we are taking the square root below, indicating
            %  that K is a linear power term.
            [M,N] = size(LOS);
            [L,P] = size(NLOS);
            if ~(M == L) || ~(N == P)
                error('Sizes of LOS and NLOS channels must be equal.');
            end
            if K < 0
                error('Rician factor must be at least 0.');
            end
            H = sqrt(K/(K+1)) .* LOS + sqrt(1/(K+1)) .* NLOS;
        end
        
        function H = channel_realization(obj)
            % CHANNEL_REALIZATION Realizes a random instance of the channel
            % matrix based on the channel object's parameters. In this
            % case, the LOS and NLOS channels are realized and then summed
            % together in a Rician fashion.
            %
            % Usage:
            %  H = CHANNEL_REALIZATION()
            %
            % Returns:
            %  H: the resulting channel matrix
            num_taps = obj.nlos_channel.num_taps;
            H = zeros(num_taps,obj.Nr,obj.Nt);
            H_LOS = obj.los_channel.channel_realization();
            H_NLOS = obj.nlos_channel.channel_realization();
            for i = 1:num_taps
                H(i,:,:) = obj.rician_add(H_LOS,squeeze(H_NLOS(i,:,:)),obj.rician_factor);
            end
            obj.set_channel_matrix(H);
        end
        
        function T = fft_channel_matrix_sequence(obj,K)
            [L,M,N] = size(obj.H);
            if nargin < 2 || isempty(K)
                % K = 2^nextpow2(L);
                K = L;
            end
            T = zeros(L,M,N);
            for k = 1:K
                for d = 1:L
                    H = obj.H(d,:,:);
                    T(k,:,:) = T(k,:,:) + H .* exp(-1j*2*pi*k*d/K);
                end
            end
        end
   end
end