classdef full_duplex_node < node
    properties
        bfc_options; % beamforming cancellation method
        si_csi; % self-interference channel state information
    end
    methods
        function obj = full_duplex_node(name)
            % FULL_DUPLEX_NODE Creates an instance of a full-duplex node
            % object.
            %
            % Usage:
            %  obj = FULL_DUPLEX_NODE(name)
            %
            % Args:
            %  name: an optional human-readable name for the object
            %
            % Returns:
            %  obj: an object representing a full-duplex node
            if nargin < 1 || isempty(name)
                name = 'full-duplex-node';
            end
            obj.name = name;
            obj.type = 'full-duplex';
            obj.set_marker('bx'); % blue marker denotes full-duplex
        end
        
        function set_self_interference_channel_state_information(obj,csi)
            obj.si_csi = csi;
        end

        function transmit_beamform(obj)
            strategy = obj.transmit_strategy.strategy;
            if strcmp(strategy,'eigen')
                obj.transmit_beamform_eigen();
            elseif strcmp(strategy,'bfc')
                obj.transmit_beamform_bfc();
            elseif strcmp(strategy,'rzf')
                obj.transmit_beamform_rzf();
            else
                error('Unfamiliar transmit strategy.');
            end
            % obj.tx.impose_beamformer_consraints();
            obj.tx.impose_beamformer_power_constraint();
        end
        
        function receive_beamform(obj)
            strategy = obj.receive_strategy.strategy;
            if strcmp(strategy,'eigen')
                obj.receive_beamform_eigen();
            elseif strcmp(strategy,'bfc')
                obj.receive_beamform_bfc();
            elseif strcmp(strategy,'lmmse')
                obj.receive_beamform_lmmse();
            else
                error('Unfamiliar receive strategy.');
            end
            % obj.rx.impose_beamformer_consraints();
            % obj.rx.impose_beamformer_power_constraint();
        end
        
        function receive_beamform_eigen(obj)
            W = obj.rx.get_eigen_combiner();
            if strcmp(obj.rx.type,'hybrid')
                obj.rx.hybrid_approximation(W);
            else
                obj.rx.set_combiner(W);
            end
        end
        
        function receive_beamform_lmmse(obj)
            if strcmp(obj.rx.type,'hybrid')
                W = obj.rx.get_eigen_combiner();
                [~,RF,BB] = obj.rx.hybrid_approximation(W);
                obj.transmit_beamform();
                F_int = obj.tx.precoder;
                H_des = RF' * obj.receive_csi.H;
                snr_des = obj.receive_csi.snr;
                H_int = RF' * obj.si_csi.H * F_int;
                snr_int = obj.si_csi.snr;
                Nrf = obj.rx.num_rf_chains;
                I = eye(Nrf);
                BB = inv(H_des * H_des' + H_int * H_int' + 2 * Nrf / snr_des * I) * H_des;
                BB = BB(:,1:obj.rx.num_streams);
                obj.rx.set_combiner(RF*BB);
            else
                obj.transmit_beamform();
                F_int = obj.tx.precoder;
                H_des = obj.receive_csi.H;
                snr_des = obj.receive_csi.snr;
                H_int = obj.si_csi.H * F_int;
                snr_int = obj.si_csi.snr;
                Nr = obj.rx.Nr;
                I = eye(Nr);
                W = inv(H_des * H_des' + H_int * H_int' + 2 * Nr / snr_des * I) * H_des;
                W = W(:,1:obj.rx.num_streams);
                obj.rx.set_combiner(W);
            end
        end
        
        function transmit_beamform_rzf(obj)
            obj.receive_beamform();
            W_int = obj.rx.combiner;
            if strcmp(obj.tx.type,'hybrid')
                F = obj.tx.get_eigen_precoder();
                [~,RF,BB] = obj.tx.hybrid_approximation(F);
                disp(['Eigen Frobenius    : ' num2str(norm(F,'fro')^2)]);
                disp(['Hyb Frobenius error: ' num2str(norm(RF*BB - F,'fro')^2)]);
                H_des = obj.transmit_csi.H * RF;
                W = obj.rx.get_eigen_combiner(H_des);
                H_des = W' * H_des;
                snr_des = obj.transmit_csi.snr;
                H_int = W_int' * obj.si_csi.H * RF;
                snr_int = obj.si_csi.snr;
                Nt = obj.tx.Nt;
                Nrf = obj.tx.num_rf_chains;
                I = eye(Nrf);
                BB = inv(H_des' * H_des + snr_int/snr_des * H_int' * H_int + Nrf / snr_des * I) * H_des';
                BB = BB(:,1:obj.tx.num_streams);
                obj.tx.set_precoder(RF * BB);
                disp(['RZF Frobenius error: ' num2str(norm(RF*BB - F,'fro')^2)]);
            else
                H_des = obj.transmit_csi.H;
                snr_des = obj.transmit_csi.snr;
                H_int = obj.si_csi.H;
                snr_int = obj.si_csi.snr;
                Nt = obj.tx.Nt;
                I = eye(Nt);
                F = inv(H_des' * H_des + snr_int/snr_des * H_int' * H_int + 2 * Nt / snr_des * I) * H_des;
                F = F(:,1:obj.tx.num_streams);
                obj.tx.set_precoder(F);
            end
        end
        
        function transmit_beamform_rzf_v2(obj)
            obj.receive_beamform();
            W_int = obj.rx.combiner;
            if strcmp(obj.tx.type,'hybrid')
                % F = obj.tx.get_eigen_precoder();
                % [~,RF,BB] = obj.tx.hybrid_approximation(F);
                H_des = obj.transmit_csi.H;
                W = obj.rx.get_eigen_combiner(H_des);
                H_des = W' * H_des;
                snr_des = obj.transmit_csi.snr;
                H_int = W_int' * obj.si_csi.H;
                snr_int = obj.si_csi.snr;
                Nt = obj.tx.Nt;
                Nrf = obj.tx.num_rf_chains;
                % I = eye(Nrf);
                I = eye(Nt);
                F = inv(H_des' * H_des + snr_int/snr_des * H_int' * H_int + Nt / snr_des * I) * H_des';
                F = F(:,1:obj.tx.num_streams);
                [~,RF,BB] = obj.tx.hybrid_approximation(F);
                obj.tx.set_precoder(RF * BB);
            else
                H_des = obj.transmit_csi.H;
                snr_des = obj.transmit_csi.snr;
                H_int = obj.si_csi.H;
                snr_int = obj.si_csi.snr;
                Nt = obj.tx.Nt;
                I = eye(Nt);
                F = inv(H_des' * H_des + snr_int/snr_des * H_int' * H_int + 2 * Nt / snr_des * I) * H_des;
                F = F(:,1:obj.tx.num_streams);
                obj.tx.set_precoder(F);
            end
        end
        
        function transmit_beamform_eigen(obj)
            F = obj.tx.get_eigen_precoder();
            if strcmp(obj.tx.type,'hybrid')
                obj.tx.hybrid_approximation(F);
            else
                obj.tx.set_precoder(F);
            end
        end
        
        function transmit_beamform_bfc(obj)
            F = obj.tx.get_eigen_precoder();
            H_SI = obj.si_csi.H;
            obj.receive_beamform();
            W = obj.rx.combiner;
            if strcmp(obj.tx.type,'hybrid')
                wt = obj.transmit_strategy.bfc_weight;
                [~,RF,BB] = obj.tx.hybrid_approximation(F);
                P = get_projection_matrix(null(W' * H_SI * RF));
                Y = P * BB;
                E = BB - Y;
                F = RF * (Y + (1-wt) * E);
            else
                P = get_projection_matrix(null(W' * H_SI));
                F = P * F;
            end
            obj.tx.set_precoder(F);
        end
        
        function transmit_beamform_bfc_v2(obj)
            F = obj.tx.get_eigen_precoder();
            H_SI = obj.si_csi.H;
            obj.receive_beamform();
            W = obj.rx.combiner;
            F = get_projection_matrix(null(W' * H_SI)) * F;
            if strcmp(obj.tx.type,'hybrid')
                wt = obj.transmit_strategy.bfc_weight;
                [~,RF,BB] = obj.tx.hybrid_approximation(F);
                P = get_projection_matrix(null(W' * H_SI * RF));
                Y = P * BB;
                E = BB - Y;
                F = RF * (Y + (1-wt) * E);
            else
                P = get_projection_matrix(null(W' * H_SI));
                F = P * F;
            end
            obj.tx.set_precoder(F);
        end
        
        function set_bfc_options(obj,opt)
            % SET_BFC_OPTIONS Sets the beamforming cancellation struct,
            % which should include whatever params are necessary for that
            % method.
            %
            % Usage:
            %  SET_BFC_OPTIONS(opt)
            %
            % Args:
            %  opt: a struct of beamforming cancellation options; specific
            %       struct parameters depends on the method of choice
            obj.bfc_options = opt;
        end
        
        function enable_bfc(obj)
            % ENABLE_BFC Enables beamforming cancellation based on the
            % current BFC options.
            % set precoder
            % set combiner
        end
        
        function W = lmmse_combiner(obj)
            % pass
        end
    end
end