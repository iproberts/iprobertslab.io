classdef half_duplex_node < node
    properties
        
    end
    methods
        function obj = half_duplex_node(name)
            % HALF_DUPLEX_NODE Creates an instance of a half-duplex node
            % object.
            %
            % Usage:
            %  obj = HALF_DUPLEX_NODE(name)
            %
            % Args:
            %  name: an optional human-readable name for the object
            %
            % Returns:
            %  obj: an object representing a half-duplex node
            if nargin < 1 || isempty(name)
                name = 'half-duplex-node';
            end
            obj.name = name;
            obj.type = 'half-duplex';
            obj.set_marker('rx'); % red marker denotes half-duplex
        end

    end
end