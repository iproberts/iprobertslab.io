classdef mmwave_receiver < receiver
    % MMWAVE_RECEIVER A millimeter-wave MIMO receiver.
    properties
        num_rf_chains; % number of RF chains
        combiner_digital; % digital combining matrix
        combiner_analog; % analog combining matrix
        phase_shifter_resolution; % analog phase shifter resolution (bits)
        amplitude_control; % assume constant amplitude by default
        amplitude_resolution; % analog amplitude resolution (bits)
        hybrid_approximation_method; % hybrid approximation method
    end
    methods
        function obj = mmwave_receiver(name)
            % MMWAVE_RECEIVER Creates an instance of a millimeter-wave 
            % receiver object.
            if nargin < 1 || isempty(name)
                name = 'mmwave-receiver';
            end
            obj.name = name;
            obj.type = 'hybrid';
            obj.set_amplitude_control(false);
        end
        
        function set_num_rf_chains(obj,Nrf)
            % SET_NUM_RF_CHAINS Sets the number of RF chains in the
            % receiver's hybrid beamforming structure.
            %
            % Usage:
            %  SET_NUM_RF_CHAINS(Nrf)
            % 
            % Args:
            %  Nrf: number of RF chains
            if Nrf < obj.num_streams
                warning('Number of RF chains is less than number of data streams.');
            end
            obj.num_rf_chains = Nrf;
        end
        
        function set_combiner_analog(obj,W)
            % SET_COMBINER_ANALOG Sets the analog combining matrix.
            %
            % Usage:
            %  SET_COMBINER_ANALOG(W)
            % 
            % Args:
            %  W: analog combining matrix
            [Nr,Nrf] = size(W);
            if ~(Nrf == obj.num_rf_chains)
                warning('The number of columns in W should be equal to the number of RF chains.');
            end
            if ~(Nr == obj.Nr)
                warning('The number of rows in W should be equal to the number of receive antennas.');
            end
            obj.combiner_analog = W;
        end
        
        function set_combiner_digital(obj,W)
            % SET_COMBINER_DIGITAL Sets the digital combiner.
            %
            % Usage:
            %  SET_COMBINER_DIGITAL(W)
            %
            % Args:
            %  W: digital combining matrix
            obj.combiner_digital = W;
        end
        
        function impose_analog_beamformer_power_constraint(obj)
            % IMPOSE_ANALOG_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the analog beamformer such that each beamformer 
            % has Frobenius norm  of one.
            %
            % Usage:
            %  IMPOSE_ANALOG_BEAMFORMER_POWER_CONSTRAINT()
            W = obj.combiner_analog;
            p = 1;
            Ns = obj.num_streams;
            for i = 1:Ns
                w = W(:,i);
                n = norm(w);
                if n > 0
                    W(:,i) = w ./ n .* p;
                end
            end
            obj.combiner_analog = W;
        end
            
        function impose_beamformer_power_constraint(obj)
            % IMPOSE_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the digital beamformer such that each stream's
            % beamformer has Frobenius norm  of square root of the number
            % of transmit antennas.
            %
            % Usage:
            %  IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()
            W = obj.combiner;
            BB = obj.combiner_digital;
            p = 1;
            Ns = obj.num_streams;
            for i = 1:Ns
                w = W(:,i);
                n = norm(w,'fro');
                if n > 0
                    W(:,i) = w ./ n .* p;
                    BB(:,i) = BB(:,i) ./ n .* p;
                end
            end
            obj.combiner = W;
            obj.combiner_digital = BB;
        end
        
        function impose_digital_beamformer_power_constraint(obj)
            % IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the digital beamformer such that each stream's
            % beamformer has Frobenius norm of square root of the number
            % of receive antennas.
            %
            % Usage:
            %  IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()
            W = obj.combiner_digital;
            
            % !!! fix this !!!
            p = sqrt(obj.Nr);
            p = 1;
            % !!!!!!!!!!!!!!!!
            
            Ns = obj.num_streams;
            for i = 1:Ns
                w = W(:,i);
                n = norm(w,'fro');
                if n > 0
                    W(:,i) = w ./ n .* p;
                end
            end
            obj.combiner_digital = W;
        end
        
        function set_phase_shifter_resolution(obj,bits)
            % SET_PHASE_SHIFTER_RESOLUTION Sets the resolution (in bits) of
            % the phase shifters in the analog beamformer.
            %
            % Usage:
            %  SET_PHASE_SHIFTER_RESOLUTION(bits)
            %
            % Args:
            %  bits: number of bits of phase control
            obj.phase_shifter_resolution = bits;
        end
        
        function set_amplitude_control(obj,option)
            % SET_AMPLITUDE_CONTROL Sets whether or not there is amplitude
            % control in the analog beamformer. If there is no amplitude
            % control, the analog beamformer must abide by a constant
            % amplitude constraint.
            %
            % Usage:
            %  SET_AMPLITUDE_CONTROL(option)
            %
            % Args:
            %  option: a boolean option representing if the analog
            %  beamformer has amplitude control or not
            obj.amplitude_control = option;
        end
        
        function set_amplitude_resolution(obj,bits)
            % SET_AMPLITUDE_RESOLUTION Sets the resolution (in 
            % bits) of the amplitude gain in the analog beamformer.
            %
            % Usage:
            %  SET_AMPLITUDE_RESOLUTION(bits)
            %
            % Args:
            %  bits: number of bits of amplitude control
            if ~obj.amplitude_control
                warning('The receiver amplitude control is currently set to false. Setting resolution anyway.');
            end
            obj.amplitude_resolution = bits;
        end
        
        function set_hybrid_approximation_method(obj,method)
            % SET_HYBRID_APPROXIMATION_METHOD Sets the method to use during
            % hybrid approximation of a fully-digital beamformer.
            %
            % Usage:
            %  SET_HYBRID_APPROXIMATION_METHOD(method)
            % 
            % Args:
            %  method: a string declaring which method to use
            obj.hybrid_approximation_method = method;
        end
                
        function [X,RF,BB,err] = hybrid_approximation(obj,W)
            % HYBRID_APPROXIMATION Performs hybrid approximation of a
            % fully-digital beamforming matrix based on the hybrid
            % approximation method and any associated options.
            %
            % Usage:
            %  [X,RF,BB,err] = HYBRID_APPROXIMATION(W,opt)
            %
            % Args:
            %  W: fully-digital combining matrix
            %  opt: struct of options specific to the hybrid approximation
            %       method
            %
            % Returns:
            %  X: the effective fully-digital beamforming matrix
            %  RF: the resulting analog beamforming matrix
            %  BB: the resulting digital beamforming matrix
            %  err: the squared Frobenius norm of the error in hyrbrid
            %       approximation
            % 
            % Notes:
            %  The analog and digital combiners are set after
            %  approximation.
            opt = obj.hybrid_approximation_method;
            method = opt.method;
            if strcmp(method,'omp')
                A = opt.codebook;
                [X,RF,BB] = obj.omp_based_hybrid_approximation(W,A);
            else
                error('Invalid hybrid approximation method.');
            end
            err = norm((W-X).^2,'fro');
            obj.set_combiner_analog(RF);
            obj.set_combiner_digital(BB);
            obj.set_combiner(X);
        end
        
        function [X,X_RF,X_BB] = omp_based_hybrid_approximation(obj,W,A)
            % OMP_BASED_HYBRID_APPROXIMATION Spatially sparse beamforming
            % design via orthogonal matching pursuit (OMP).
            % 
            % Usage:
            %  [X,X_RF,X_BB] = OMP_BASED_HYBRID_APPROXIMATION(W,A)
            %
            % Args:
            %  W: desired fully-digital beamformer
            %  A: predefined RF beamforming vectors as columns (codebook)
            %
            % Returns:
            %  X: the effective fully-digital beamformer after hybrid
            %     approximation
            %  X_RF: the analog beamformer after hybrid approximation
            %  X_BB: the digital beamformer after hybrid approximation
            P = 1; % power constraint
            C = eye(obj.Nr);
            Nrf = obj.num_rf_chains;
            X_res = W;
            X_RF = [];
            for r = 1:Nrf
                PHI = A' * C' * X_res;
                k = argmax(diag(PHI * PHI'));
                X_RF = [X_RF A(:,k)];
                X_BB = inv(X_RF' * C' * C * X_RF) * X_RF' * C' * W;
                num = W - C * X_RF * X_BB;
                X_res = num / norm(num,'fro');
            end
            X_BB = sqrt(P) * X_BB / norm(X_RF * X_BB,'fro');
            X = X_RF * X_BB;
        end
        
        function show_analog_beamformer(obj,idx)
            % SHOW_ANALOG_BEAMFORMER Plots one of the analog beamformers.
            %
            % Usage:
            %  SHOW_ANALOG_BEAMFORMER()
            %  SHOW_ANALOG_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the analog
            %  combining matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            w = obj.combiner_analog(:,idx);
            obj.array.show_beamformer_pattern(w);
        end
        
        function show_effective_beamformer(obj,idx)
            % SHOW_EFFECTIVE_BEAMFORMER Plots one of the effective 
            % beamformers (combination of analog and digital).
            %
            % Usage:
            %  SHOW_EFFECTIVE_BEAMFORMER()
            %  SHOW_EFFECTIVE_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the effective
            %  combining matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            W = obj.combiner_analog * obj.combiner_digital;
            w = W(:,idx);
            obj.array.show_beamformer_pattern(w);
        end
    end
end