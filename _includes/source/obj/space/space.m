classdef space < handle
    properties
        name;
        nodes;
        num_nodes;
        channels;
        snrs;
        links; % matrix indicating who is talking to who
        propagation_velocity = 1;
        carrier_frequency = 1;
        carrier_wavelength = 1;
        path_loss_exponent = 0;
    end
    methods
        function obj = space(name)
            % SPACE Creates a space object.
            %
            % Usage:
            %  obj = SPACE()
            %  obj = SPACE(name)
            %
            % Args:
            %  name: an optional name for the object
            %
            % Returns:
            %  obj: an object representing a space
            if nargin < 1 || isempty(name)
                name = 'space';
            end
            obj.name = name;
            obj.num_nodes = 0;
            obj.nodes = {};
            obj.channels = {}; % init to empty cell
            obj.snrs = [];
            obj.links = [];
            % obj.channels = [];
        end
        
        function add_node(obj,node)
            % ADD_NODE Adds a node to the space.
            %
            % Usage:
            %  ADD_NODE(target)
            %
            % Args:
            %  node: a node object to add to the space
            [~,idx_log] = find_cell(obj.nodes,node);
            if any(idx_log)
                node_already_exists = true; % !!! BROKEN DUE TO CELL !!!
            else
                node_already_exists = false;
            end
            if ~node_already_exists
                obj.num_nodes = obj.num_nodes + 1;
                % obj.nodes = [obj.nodes node];
                N = obj.num_nodes;
                M = cell(N,1);
                S = obj.nodes;
                M(1:N-1,1) = S;
                M{N,1} = node;
                obj.nodes = M;
                
                M = cell(N);
                % M = zeros(N);
                % M(N,N) = channel();
                S = obj.channels;
                M(1:N-1,1:N-1) = S;
                obj.channels = M;
                
                S = obj.snrs;
                M = zeros(N,N);
                M(1:N-1,1:N-1) = S;
                obj.snrs = M;
                
                S = obj.links;
                M = zeros(N,N);
                M(1:N-1,1:N-1) = S;
                obj.links = M;
            else
                warning('Node already exists in space, not duplicating it.');
            end
        end
        
        function add_nodes(obj,nodes)
            % ADD_NODES Adds a vector of nodes to the space.
            %
            % Usage:
            %  ADD_NODES(nodes)
            %
            % Args:
            %  nodes: a vector of node objects
            N = length(nodes);
            for i = 1:N
                if iscell(nodes)
                    obj.add_node(nodes{i});
                else
                    obj.add_node(nodes(i));
                end
            end
        end
        
        function remove_node(obj,idx)
            % REMOVE_NODE Removes a node from the space.
            %
            % Usage:
            %  REMOVE_NODE()
            %  REMOVE_NODE(idx)
            %
            % Args:
            %  idx: optional node index to remove from the space (default
            %       is the last node in the space)
            if nargin < 2
                idx = obj.num_nodes;
            end
            if obj.num_nodes < 1
                error('There are no nodes in the space to remove.');
            elseif obj.num_nodes < idx
                error('The input index is greater than the number of nodes in the space.');
            end
            if isnumeric(idx)
                if idx < 1 || idx > obj.num_nodes
                    error('Invalid node index.');
                end
            elseif isobject(idx)
                idx = find(idx == obj.nodes);
                if isempty(idx)
                    error('Node not found in space.');
                end
            else
                error('Invalid node format.');
            end
            obj.nodes = {obj.nodes{1:idx-1} obj.nodes{idx+1:end}};
            obj.num_nodes = obj.num_nodes - 1;
            N = obj.num_nodes;
            S = obj.channels;
            v = [(1:idx-1) (idx+1):N+1];
            M = S(v,v);
            obj.channels = M;
            
            S = obj.snrs;
            M = S(v,v);
            obj.snrs = M;
            
            S = obj.links;
            M = S(v,v);
            obj.links = M;
        end
        
        function remove_all_nodes(obj)
            % REMOVE_ALL_NODES Removes all nodes from the space.
            %
            % Usage:
            %  REMOVE_ALL_NODES()
            obj.nodes = {};
            obj.num_nodes = 0;
            obj.channels = [];
            obj.snrs = [];
            obj.links = [];
        end
        
        function idx = get_node_index(obj,nx)
            if isnumeric(nx)
                if nx < 1 || nx > obj.num_nodes
                    error('Invalid node index.');
                else
                    idx = nx; % !!! could be non-integer !!!
                end
            elseif isobject(nx) && isobject(nx)
                idx = find_cell(nx,obj.nodes);
                if isempty(idx)
                    error('Node not found in space.');
                end
            else
                error('Invalid node formats.');
            end
        end
        
        function set_channel(obj,rx,tx,H)
            % SET_CHANNEL Sets the channel matrix between two nodes.
            %
            % Usage:
            %  SET_CHANNEL(idx_tx,idx_rx,H)
            %
            % Args:
            %  idx_tx: transmit node index
            %  idx_rx: receive node index
            %  H: channel matrix
            idx_rx = obj.get_node_index(rx);
            idx_tx = obj.get_node_index(tx);
            Nr = H.Nr;
            Nt = H.Nt;
            if ~(Nr == obj.nodes{idx_rx}.rx.Nr)
                error('Number of receiver antennas in channel object does not equal the number of receive antennas at the receive node.');
            end
            if ~(Nt == obj.nodes{idx_tx}.tx.Nt)
                error('Number of transmit antennas in channel object does not equal the number of transmit antennas at the transmit node.');
            end
            % obj.channels(idx_rx,idx_tx) = H;
            obj.channels{idx_rx,idx_tx} = H;
        end
        
        function set_snr(obj,rx,tx,snr,unit)
            % SET_SNR Sets the SNR for the link between a transmit-receive
            % pair.
            %
            % Usage:
            %  SET_SNR(rx,tx,snr)
            %  SET_SNR(rx,tx,snr,unit)
            %
            % Args:
            %  rx: node object (or index) of the receiver
            %  tx: node object (or index) of the transmitter
            %  snr: SNR of the link (linear power)
            %  unit: the unit of snr ('linear','dB')
            if nargin < 5 || isempty(unit)
                unit = 'linear';
            end
            if strcmp(unit,'dB')
                snr = 10^(snr/10);
            end
            idx_rx = obj.get_node_index(rx);
            idx_tx = obj.get_node_index(tx);
            obj.snrs(idx_rx,idx_tx) = snr;
        end
        
        function set_all_snrs(obj,snr)
            % SET_ALL_SNRS Sets all link SNRs to the same value.
            %
            % Usage:
            %  SET_ALL_SNRS(snr)
            %
            % Args:
            %  snr: SNR value (linear power)
            obj.snrs = ones(obj.num_nodes) .* snr;
        end
        
        function populate_snrs_from_path_loss(obj)
            % POPULATE_SNRS_FROM_PATH_LOSS Sets all of the link SNRs based 
            % on the path loss, transmit power, and noise variance
            % associated with each transmit-receive pair.
            %
            % Usage:
            %  POPULATE_SNRS_FROM_PATH_LOSS()
            %
            % Notes:
            %  The SNR we define is
            %
            %    SNR = (transmit power) * (path gain)^2 / (noise variance)
            % 
            %  Computes the path loss based on the coordinates of the
            %  transmit and receive nodes.
            %
            %  Retrieves the transmit power from the transmit node.
            % 
            %  Retrieves the noise variance from the receive node.
            wavelength = obj.carrier_wavelength;
            for idx_tx = 1:obj.num_nodes
                idx_rx = find(obj.links(:,idx_tx) == 1); % which links have tx as transmitter
                for j = 1:length(idx_rx)
                    dist = obj.get_relative_position(idx_tx,idx_rx(j));
                    G = obj.get_large_scale_gain(dist,wavelength);
                    Ptx = obj.nodes{idx_tx}.tx.transmit_power;
                    noise_var = obj.nodes{idx_rx(j)}.rx.noise_psd_watts_Hz;
                    snr = Ptx * G^2 / noise_var;
                    obj.set_snr(idx_rx(j),idx_tx,snr);
                end
            end
        end
        
        function populate_desired_channels(obj,H)
            % POPULATE_DESIRED_CHANNELS Populates each transmit-receive
            % pair's channel as the same channel object. Only channels
            % between two different devices are populated (i.e., "desired"
            % channels). The channels between a node's transmitter and its
            % receiver (i.e., self-interference channel) is not populated.
            %
            % Usage:
            %  POPULATE_DESIRED_CHANNELS(H)
            %
            % Args:
            %  H: channel object to describe the desired channels
            %
            % Issue:
            %  !!! Need to set the transmit array and receive array at each
            %  channel uniquely !!!
            for i = 1:obj.num_nodes % transmit index
                for j = 1:obj.num_nodes % receive index
                    if ~(i == j)
                        atx = obj.nodes{i}.tx.array;
                        arx = obj.nodes{j}.rx.array;
                        H = copy_object(H);
                        H.set_transmit_array(atx);
                        H.set_receive_array(arx);
                        obj.set_channel(j,i,H);
                    end
                end
            end
        end
        
        function populate_self_interference_channels(obj,H)
            % POPULATE_SELF_INTERFERENCE_CHANNELS Populates the channels
            % between each deivce's transmitter and its own receiver.
            %
            % Usage:
            %  POPULATE_SELF_INTERFERENCE_CHANNELS(H)
            %
            % Args:
            %  H: channel object to describe the self-interference channels
            %
            % Issue:
            %  !!! Need to set the transmit array and receive array at each
            %  channel uniquely !!!
            for i = 1:obj.num_nodes
                obj.set_channel(i,i,H);
            end
        end
        
        function channel_realizations(obj)
            % CHANNEL_REALIZATIONS Realizes a random channel for each
            % channel in the space.
            %
            % Usage: 
            %  CHANNEL_REALIZATIONS()
            for i = 1:obj.num_nodes
                for j = 1:obj.num_nodes
                    if ~isempty(obj.channels{j,i})
                        obj.channels{j,i}.channel_realization();
                    end
                end
            end
        end
        
        function create_link(obj,rx,tx)
            % CREATE_LINK Establishes a link between a transmit-receive
            % pair.
            %
            % Usage:
            %  CREATE_LINK(rx,tx)
            %
            % Args:
            %  rx: node object (or index) of the receiver
            %  tx: node object (or index) of the transmitter
            %
            % Note:
            %  The links matrix is square. It is 1 where there exists a 
            %  link between two nodes and 0 where there does not exist a
            %  link between two nodes.
            %
            %  A link indicates desired communication between a transmit
            %  node and receive node.
            idx_rx = obj.get_node_index(rx);
            idx_tx = obj.get_node_index(tx);
            obj.links(idx_rx,idx_tx) = 1;
        end
               
        function csi = get_transmit_channel_state_information(obj,tx)
            % GET_TRANSMIT_CHANNEL_STATE_INFORMATION Returns the CSI
            % (channel matrix and SNR) of the channel corresponding to the
            % transmit node's desired receive node.
            %
            % Usage:
            %  csi = GET_TRANSMIT_CHANNEL_STATE_INFORMATION(tx)
            %
            % Args:
            %  tx: node object (or index) of the transmitting device            
            % 
            % Returns:
            %  csi: a struct containing the channel matrix (H) and the SNR
            %  of that channel (snr)
            %
            % Note:
            %  Only supports SU-MIMO.
            idx_tx = obj.get_node_index(tx);
            idx_rx = find(obj.links(:,idx_tx) == 1); % which links have tx as transmitter
            if length(idx_rx) > 1
                warning('More than one receiver associated with transmitter. Using first one only.');
                idx_rx = idx_rx(1);
            end
            if isempty(idx_rx)
                H = [];
                snr = [];
            else
                H = obj.channels{idx_rx,idx_tx}.get_channel_matrix();
                snr = obj.snrs(idx_rx,idx_tx);
            end
            csi.H = H;
            csi.snr = snr;
        end
        
        function csi = get_receive_channel_state_information(obj,rx)
            % GET_RECEIVE_CHANNEL_STATE_INFORMATION Returns the CSI
            % (channel matrix and SNR) of the channel corresponding to the
            % receive node's desired transmit node.
            %
            % Usage:
            %  csi = GET_RECEIVE_CHANNEL_STATE_INFORMATION(rx)
            %
            % Args:
            %  rx: node object (or index) of the receiving device            
            % 
            % Returns:
            %  csi: a struct containing the channel matrix (H) and the SNR
            %  of that channel (snr)
            %
            % Note:
            %  Only supports SU-MIMO.
            idx_rx = obj.get_node_index(rx);
            idx_tx = find(obj.links(idx_rx,:) == 1); % which links have tx as transmitter
            if length(idx_tx) > 1
                warning('More than one transmitter associated with receiver. Using first one only.');
                idx_tx = idx_tx(1);
            end
            if isempty(idx_tx)
                H = [];
                snr = [];
            else
                H = obj.channels{idx_rx,idx_tx}.get_channel_matrix();
                snr = obj.snrs(idx_rx,idx_tx);
            end
            csi.H = H;
            csi.snr = snr;
        end
        
        function csi = get_self_interference_channel_state_information(obj,nx)
            % GET_SELF_INTERFERENCE_CHANNEL_STATE_INFORMATION Returns the 
            % CSI (channel matrix and SNR) of the channel between a node's 
            % transmitter and its own receiver.
            %
            % Usage:
            %  csi = GET_RECEIVE_CHANNEL_STATE_INFORMATION(rx)
            %
            % Args:
            %  nx: node object (or index) of the device of interest       
            % 
            % Returns:
            %  csi: a struct containing the channel matrix (H) and the SNR
            %  of that channel (snr)
            nx = obj.get_node_index(nx);
            H = obj.channels{nx,nx}.get_channel_matrix();
            snr = obj.snrs(nx,nx);
            csi.H = H;
            csi.snr = snr;
        end
        
        function provide_channel_state_information(obj)
            % PROVIDE_CHANNEL_STATE_INFORMATION Gets the transmit and
            % receive CSI for each node and passes it to it.
            %
            % Usage:
            %  PROVIDE_CHANNEL_STATE_INFORMATION()
            %
            % Note:
            %  If the device is a full-duplex device, it will also provide
            %  the CSI of the self-interference channel.
            for i = 1:obj.num_nodes
                % transmit csi
                csi = obj.get_transmit_channel_state_information(i);
                obj.nodes{i}.set_transmit_csi(csi);
                % receive csi
                csi = obj.get_receive_channel_state_information(i);
                obj.nodes{i}.set_receive_csi(csi);
                % full-duplex csi
                if strcmp(obj.nodes{i}.type,'full-duplex')
                    csi = obj.get_self_interference_channel_state_information(i);
                    obj.nodes{i}.set_self_interference_channel_state_information(csi);
                end
            end
        end
        
        function configure_beamformers(obj)
            % CONFIGURE_BEAMFORMERS Configures the transmit precoding
            % matrix and receive combining matrix at each node based on its
            % beamforming settings.
            %
            % Usage:
            %  CONFIGURE_BEAMFORMERS()
            idx = find(obj.links == 1);
            [r,c] = ind2sub(size(obj.links),idx);
            for i = 1:length(c)
                obj.nodes{c(i)}.transmit_beamform();
            end
            for i = 1:length(r)
                obj.nodes{r(i)}.receive_beamform();
            end
        end
        
        function set_propagation_velocity(obj,val)
            % SET_PROPAGATION_VELOCITY Sets the propagation velocity of the
            % environment and informs each radar in the space of this.
            %
            % Usage:
            %  SET_PROPAGATION_VELOCITY(val)
            %
            % Args:
            %  val: propagation velocity (meters/sec)
            obj.propagation_velocity = val;
            obj.carrier_wavelength = val / obj.carrier_frequency;
        end
        
        function set_path_loss_exponent(obj,ple)
            % SET_PATH_LOSS_EXPONENT Sets the path loss exponent of free
            % space propagation.
            %
            % Usage:
            %  SET_PATH_LOSS_EXPONENT(ple)
            %
            % Args:
            %  ple: path loss exponent of free space propagation
            obj.path_loss_exponent = ple;
        end
        
        function set_carrier_frequency(obj,fc)
            % SET_CARRIER_FREQUENCY Sets the carrier frequency of the
            % propagations in the space.
            %
            % Usage:
            %  SET_CARRIER_FREQUENCY(fc)
            %
            % Args:
            %  fc: carrier frequency (Hz)
            obj.carrier_frequency = fc;
            obj.carrier_wavelength = obj.propagation_velocity / fc;
        end
        
        function [rg,az,el] = get_relative_position(obj,idx_1,idx_2)
            % GET_RELATIVE_POSITION Returns the relative position
            % parameters (range, azimuth, and elevation) between two nodes.
            %
            % Usage:
            %  [rg,az,el] = GET_RELATIVE_POSITION(idx_1,idx_2)
            %
            % Args:
            %  idx_1: the index of the first node of interest
            %  idx_2: the index of the second node of interest
            %
            % Returns:
            %  rg: the range (in meters) between the nodes
            %  az: the azimuth angle (in radians) between the nodes
            %  el: the elevation angle (in radians) between the nodes
            %
            % Note:
            %  We are finding the position of node idx_2 relative to the
            %  the position of node idx_1.
            %
            %  Our convention is that the elevation angle is from the x-y
            %  plane, increasing in the +z direction. It extends from -pi/2
            %  to pi/2.
            %
            %  Our convention is that the azimuth angle is from the +y-z
            %  plane, increasing toward the +x axis and decreasing toward
            %  the -x axis. It extends from -pi to pi.
            dx = obj.nodes{idx_2}.x - obj.nodes{idx_1}.x;
            dy = obj.nodes{idx_2}.y - obj.nodes{idx_1}.y;
            dz = obj.nodes{idx_2}.z - obj.nodes{idx_1}.z;
            rg = sqrt(dx.^2 + dy.^2 + dz.^2);
            az = atan2(dx,dy);
            el = atan2(dz,sqrt(dx.^2 + dy.^2));
        end
        
        function gain = get_free_space_gain(obj,dist,wavelength)
            % GET_FREE_SPACE_GAIN Returns the free space gain (in
            % amplitude) for travel over a given distance and wavelength
            % (both in same unit). If no wavelength is given, it is assumed
            % that the distance is in units of wavelength.
            %
            % Usage:
            %  gain = GET_FREE_SPACE_GAIN(dist)
            %  gain = GET_FREE_SPACE_GAIN(dist,wavelength)
            %
            % Args:
            %  dist: propagation distance (in same units as wavelength)
            %  wavelength: carrier wavelength of the propagating wave
            %  (default of 1)
            %
            % Returns:
            %  gain: the linear amplitude gain of the signal caused by free
            %        space propagation
            %
            % Notes:
            %  This is a gain not a loss factor! Invert it to get the loss!
            if nargin < 3
                wavelength = 1;
            end
            loss = sqrt((4*pi*dist/wavelength)^(obj.path_loss_exponent));
            if loss > 0
                gain = 1 / loss;
            else
                gain = 1;
            end
        end
        
        function gain = get_large_scale_fading_gain(obj)
            % GET_LARGE_SCALE_FADING_GAIN Returns the large-scale fading
            % gain (in linear amplitude).
            %
            % Usage:
            %  gain = GET_LARGE_SCALE_FADING_GAIN()
            %
            % Returns:
            %  gain: the large-scale fading gain
            %
            % To do:
            %  Make functional if desired. Could represent lognormal
            %  shadowing. This is meant to represent large-scale fading
            %  (like shadowing), not to represent small-scale fading (like
            %  Rayleigh fading)!
            
            % !!! TEMP !!!
            gain = 1;
            % !!!!!!!!!!!!
        end
        
        function gain = get_large_scale_gain(obj,dist,wavelength)
            % GET_LARGE_SCALE_GAIN Returns the large scale gain for
            % propagation along a distance relative to its wavelength.
            % Incorporates both free space gain and large-scale fading.
            %
            % Usage:
            %  gain = GET_LARGE_SCALE_GAIN(dist)
            %  gain = GET_LARGE_SCALE_GAIN(dist,wavelength)
            %
            % Args:
            %  dist: propagation distance (in same unit as wavelength)
            %  wavelength: the carrier wavelength of the propagating wave
            %              (in same unit as distance) (default of 1)
            %
            % Returns:
            %  gain: the large-scale linear amplitude gain as a result of
            %        free space propagation and large-scale fading
            if nargin < 3
                wavelength = 1;
            end
            a = obj.get_free_space_gain(dist,wavelength);
            b = obj.get_large_scale_fading_gain();
            gain = a * b;
        end
        
        function R = calculate_spectral_efficiencies(obj)
            % CALCULATE_SPECTRAL_EFFICIENCIES Calculates the spectral
            % efficiency on each link in the network.
            %
            % Usage:
            %  R = CALCULATE_SPECTRAL_EFFICIENCIES()
            %
            % Returns:
            %  R: a matrix containing the spectral efficiencies achieved at
            %     each link in the network
            N = obj.num_nodes;
            R = zeros(N);
            for i = 1:N
                for j = 1:N
                    if obj.links(j,i)
                        R(j,i) = obj.get_spectral_efficiency(j,i);
                    end
                end
            end
        end
        
        function R = get_spectral_efficiency_old(obj,rx,tx)
            % GET_SPECTRAL_EFFICIENCY Returns the spectral efficency
            % (Gaussian mutual information) between a transmitting node and
            % receiving node.
            %
            % !!! Need to update to account for Rs = 1/Ns * I !!!
            H = obj.channels{rx,tx}.H;
            SNR = obj.snrs(rx,tx);
            C = sqrt(SNR) .* H;
            F = obj.nodes{tx}.tx.precoder;
            W = obj.nodes{rx}.rx.combiner;
            Rs = obj.nodes{tx}.tx.Rs;
            Rn = obj.nodes{rx}.rx.Rn;
            if strcmp(obj.nodes{rx}.type,'full-duplex')
                Rs = obj.nodes{rx}.tx.Rs;
                FF = obj.nodes{rx}.tx.precoder;
                HH = obj.channels{rx,rx}.H;
                SNR = obj.snrs(rx,rx);
                CC = sqrt(SNR) .* HH;
                Q = CC * FF * Rs^(1/2);
            else
                Nr = obj.nodes{rx}.rx.Nr;
                Q = zeros(Nr);
            end
            R = calculate_mimo_gaussian_mutual_information(C,F,W,Rs,Rn,Q);
            % !!! there is a dimension mismatch when number of streams is
            % not equal in interference !!!
        end
        
        function R = get_spectral_efficiency(obj,rx,tx)
            % GET_SPECTRAL_EFFICIENCY Returns the spectral efficiency
            % achieved between a transmit-receive pair.
            %
            % Usage:
            %  R = GET_SPECTRAL_EFFICIENCY(rx,tx)
            %
            % Args:
            %  rx: node object (or index) of the receiver
            %  tx: node object (or index) of the transmitter
            %
            % Returns:
            %  R: the spectral effiency (bps/Hz)
            Rs_des = obj.nodes{tx}.tx.Rs;
            F_des = obj.nodes{tx}.tx.precoder;
            W = obj.nodes{rx}.rx.combiner;
            H_des = obj.channels{rx,tx}.H;
            snr_des = obj.snrs(rx,tx);
            Rn = obj.nodes{rx}.rx.Rn;
            des = sqrt(snr_des) * H_des * F_des * sqrt(Rs_des);
            int = sqrt(Rn);
            R = obj.calculate_gaussian_mutual_information(des,int,W);
            R = real(R); % in case a residual imaginary portion
        end
        
        function R = calculate_gaussian_mutual_information(obj,des,int,W)
            % CALCULATE_GAUSSIAN_MUTUAL_INFORMATION Computes the Gaussian
            % mutual information based on a desired signal, interference
            % signal, and combiner.
            %
            % Usage:
            %  R = CALCULATE_GAUSSIAN_MUTUAL_INFORMATION(des,int,W)
            %
            % Args:
            %  des: effective desired symbols covariance
            %  int: effective interference plus noise covariance
            %  W: effective combiner acting on desired, interference, and
            %     noise
            %
            % Returns:
            %  R: the mutual information (bps/Hz)
            [~,Ns] = size(W);
            I = eye(Ns);
            R = log2(det(I + W' * (des * des') * W * inv(W' * int * int' * W)));
        end
        
        function show2d(obj)
            % SHOW2D Plots the nodes in 2-D for your viewing pleasure.
            %
            % Usage:
            %  SHOW2D()
            figure();
            for i = 1:obj.num_nodes
                plot(obj.nodes{i}.x,obj.nodes{i}.y,obj.nodes{i}.marker,'MarkerSize',10); hold on;
            end
            xlabel('$x$ (meters)')
            ylabel('$y$ (meters)');
            grid on;
            hold off;
        end
        
        function show3d(obj)
            % SHOW3D Plots the nodes in 3-D for your viewing pleasure.
            %
            % Usage:
            %  SHOW3D()
            figure();
            for i = 1:obj.num_nodes
                plot3(obj.nodes{i}.x,obj.nodes{i}.y,obj.nodes{i}.z,obj.nodes{i}.marker,'MarkerSize',10);
                hold on;
            end
            xlabel('$x$ (meters)')
            ylabel('$y$ (meters)');
            zlabel('$z$ (meters)');
            grid on;
            hold off;
        end
    end
end
