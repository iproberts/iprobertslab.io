---
layout: default
---

![Interference As Noise](/images/banner_v01_v2_plain.svg#center, "Interference As Noise")

# Welcome!

Interference As Noise is a new podcast hosted by [Ian Roberts](https://ianproberts.com) that aims to highlight wireless technologies and the careers surrounding such. Casual interviews with industry experts, university professors, and graduate students will provide listeners with informative discussions, unique perspectives, and behind-the-scenes stories on a variety of topics. The goal of Interference As Noise is to serve its audience with technical and informative discussion on wireless topics while remaining casual, digestable, and relatively high-level.


# Where To Listen

Interference As Noise can be found as a series on Ian's YouTube channel.

Ian's channel: [{{site.youtube}}]({{site.youtube}})

Interference As Noise playlist: [{{site.playlist}}]({{site.playlist}})

For the time being, Interference As Noise is available only on YouTube. With sufficient demand, its availability may be expanded to other platforms.


# List of Episodes

{% assign eps = (site.data.episodes | sort: 'date') | reverse %}
{% for ep in eps %}
[Episode #{{ ep.number }}: {{ ep.title }}]({{ ep.link }}), {{ ep.date | date: "%B %d, %Y" }}  
{{ ep.summary }}
{% endfor %}

